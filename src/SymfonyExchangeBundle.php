<?php declare(strict_types=1);

namespace Fittinq\Symfony\Exchange;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SymfonyExchangeBundle extends Bundle
{
}
