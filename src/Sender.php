<?php declare(strict_types=1);

namespace Fittinq\Symfony\Exchange;

use Fittinq\Symfony\Authenticator\TokenService\TokenService;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use stdClass;

class Sender
{
    private HttpClientInterface $httpClient;
    private TokenService $tokenService;
    private string $exchangeUrl;

    public function __construct(
        HttpClientInterface $httpClient,
        TokenService $tokenService,
        string $exchangeUrl
    )
    {
        $this->httpClient = $httpClient;
        $this->tokenService = $tokenService;
        $this->exchangeUrl = $exchangeUrl;
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws InvalidArgumentException
     */
    public function sendToExchange(array|stdClass $object, string $source, string $event, string $target = null): void
    {
        $response = $this->httpClient->request(
            Request::METHOD_POST,
            $this->exchangeUrl . "/api/v1/message/{$source}/{$event}" . ($target ? "/{$target}" : ''),
            [
                'auth_bearer' => $this->tokenService->getToken(),
                'json' => $object
            ]
        );

        $response->getContent();
    }
}
