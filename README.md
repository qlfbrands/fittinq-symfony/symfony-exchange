# Symfony Exchange

## Introduction
The `Sender` class is responsible for sending messages to the amq-exchange service using the Symfony HttpClient.
It facilitates communication with the exchange service, allowing you to send messages to amq-exchange


## Table of contents
- [Introduction](#introduction)
- [Installation](#installation)
- [Usage](#usage)
- [Configuration](#configuration)

## Installation
```bash
composer require fittinq/symfony-exchange
```

## Usage
1. Create an instance of the Sender class, providing it with the required dependencies:
   HttpClientInterface, TokenService, and the exchange URL.

```php
use Fittinq\Symfony\Exchange\Sender;
use Fittinq\Symfony\Authenticator\TokenService\TokenService;
use Symfony\Contracts\HttpClient\HttpClientInterface;

$httpClient = new HttpClient(); // Replace with your actual HttpClient instance.
$tokenService = new TokenService(); // Replace with your TokenService instance.
$exchangeUrl = 'https://exchange-service-url'; // Replace with the actual exchange service URL.

$sender = new Sender($httpClient, $tokenService, $exchangeUrl);
```

2. Use the sendToExchange method to send a message to the exchange service. Provide it with a stdClass object,
   a source, event, and an optional target.

```php
$message = new stdClass();
$message->content = 'This is the message content';

$source = 'source';
$event = 'event';
$target = 'optional-target'; // You can omit this if not needed.

$sender->sendToExchange($message, $source, $event, $target);
```

3. The sendToExchange method constructs the request and sends it to the exchange.

## Configuration
### Environment Variables
AMQ_EXCHANGE_HOST_URL=