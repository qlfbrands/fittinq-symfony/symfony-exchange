<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Exchange;

use Fittinq\Symfony\Exchange\Sender;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Exception;
use stdClass;

class SenderMock extends Sender
{
    private array $messages = [];
    private array $isSent = [];
    private bool $shouldThrowException = false;

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct()
    {
    }

    public function throwException(): void
    {
        $this->shouldThrowException = true;
    }

    public function sendToExchange(stdClass|array $object, string $source, string $event, string $target = null): void
    {
        if ($this->shouldThrowException){
            throw new Exception();
        }

        $data = new stdClass();
        $data->object = $object instanceof stdClass ? clone $object : $object;
        $data->source = $source;
        $data->event = $event;
        $data->target = $target;

        $this->messages[] = $data;
        $this->isSent["$source:$event"] = true;
    }

    public function assertMessageIsSent(stdClass|array $object, string $source, string $event, string $target = null, int $index = 0): void
    {
        Assert::assertEquals($object, $this->messages[$index]->object);
        Assert::assertEquals($source, $this->messages[$index]->source);
        Assert::assertEquals($event, $this->messages[$index]->event);
        Assert::assertEquals($target, $this->messages[$index]->target);
    }

    public function assertNoMessageHasBeenSent(string $source, string $event, string $target = null): void
    {
        Assert::assertArrayNotHasKey("$source:$event:$target", $this->isSent);
    }
}
