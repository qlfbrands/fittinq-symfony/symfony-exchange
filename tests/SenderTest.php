<?php declare(strict_types=1);

use Fittinq\Symfony\Authenticator\TokenService\TokenServiceMock;
use Fittinq\Symfony\Exchange\Sender;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Test\Fittinq\Symfony\Mock\HttpClient\HttpClientMock;
use Test\Fittinq\Symfony\Mock\HttpClient\ResponseMock;

class SenderTest extends TestCase
{
    private HttpClientMock $senderHttpClient;
    private Sender $sender;
    private string $token = 'ksuydguawegr874rwhbafa';
    private string $exchangeUrl = 'http://exchange.com';

    protected function setUp(): void
    {
        $tokenServiceHttpClient = new HttpClientMock();
        $tokenServiceHttpClient->setUpResponse(new ResponseMock(200, [], $this->token));
        $tokenService = new TokenServiceMock();
        $tokenService->setToken($this->token);

        $this->senderHttpClient = new HttpClientMock();
        $this->senderHttpClient->setUpResponse(new ResponseMock());
        $this->sender = new Sender($this->senderHttpClient, $tokenService, $this->exchangeUrl);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function test_messageIsSentToRabbitMQWhenExchangeIsProvided(): void
    {
        $message =  new stdClass();
        $message->hello = 'world';

        $this->sender->sendToExchange($message, 'source', 'event');

        $this->senderHttpClient->expectRequestToHaveBeenMadeTo("{$this->exchangeUrl}/api/v1/message/source/event");
        $this->senderHttpClient->expectRequestToHaveBeenMadeWithJSON($message);
        $this->senderHttpClient->expectRequestToHaveBeenMadeWithAuthBearerToken($this->token);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function test_messageIsSentToRabbitMQWhenMessageIsArray(): void
    {
        $message =  new stdClass();
        $message->hello = 'world';

        $this->sender->sendToExchange([$message], 'source', 'event');

        $this->senderHttpClient->expectRequestToHaveBeenMadeTo("{$this->exchangeUrl}/api/v1/message/source/event");
        $this->senderHttpClient->expectRequestToHaveBeenMadeWithJSON([$message]);
        $this->senderHttpClient->expectRequestToHaveBeenMadeWithAuthBearerToken($this->token);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function test_messageIsSentToRabbitMQWhenQueueIsProvided(): void
    {
        $message =  new stdClass();
        $message->hello = 'world';

        $this->sender->sendToExchange($message, 'source', 'event', 'target');

        $this->senderHttpClient->expectRequestToHaveBeenMadeTo("{$this->exchangeUrl}/api/v1/message/source/event/target");
        $this->senderHttpClient->expectRequestToHaveBeenMadeWithJSON($message);
        $this->senderHttpClient->expectRequestToHaveBeenMadeWithAuthBearerToken($this->token);
    }
}
